# fishinthecalculator.me

[![builds.sr.ht status](https://builds.sr.ht/~fishinthecalculator.svg)](https://builds.sr.ht/~fishinthecalculator?)

This project contains the source for my personal website.

## Contributing

Unless otherwise stated all the files in this repository are to be considered under the GPL 3.0 or later terms. Contributions are welcome from anybody.
