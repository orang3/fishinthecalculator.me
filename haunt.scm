;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;;
;;; Some code borrowed from dthompson.us and fossandcrafts.org website
;;;
;;; Copyright © 2016-2022 Christine Lemmer-Webber <cwebber@dustycloud.org>

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt post)
             (haunt site)
             (markdown)
             (pages)
             (theme))

;;; Pages

(define %collections
  `(("Recent Blog Posts" "index.html" ,posts/reverse-chronological)))

;;; Site

(define max-entries 1024)

(site #:title "there's a fish in the calculator"
      #:domain "fishinthecalculator.me"
      #:scheme 'https
      #:default-metadata
      '((author . "Giacomo Leidi")
        (email  . "goodoldpaul@autistici.org"))
      #:readers (list commonmark-reader*)
      #:build-directory "public"
      #:builders (list (blog #:theme fishinthecalculator-theme
                             #:collections %collections
                             #:prefix "/blog")
                       index-page
                       (atom-feed #:blog-prefix "/blog"
                                  #:max-entries max-entries)
                       (static-directory "static" "static")
                       blog-page
                       projects-page
                       contacts-page
                       weblabels-page
                       (atom-feeds-by-tag)))
