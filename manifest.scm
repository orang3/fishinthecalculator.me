(use-modules (gnu packages autotools)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages rsync)
             (gnu packages texinfo)
             (guix git-download)
             (guix packages)
             (guix profiles)
             (guix utils))

(define guile-syntax-highlight*
  (let ((commit "e40cc75f93aedf52d37c8b9e4f6be665e937e21d"))
    (package
      (inherit guile-syntax-highlight)
      (version (git-version (package-version guile-syntax-highlight) "0" commit))
      (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://git.dthompson.us/guile-syntax-highlight.git")
                     (commit commit)))
               (sha256
                (base32
                 "0iqspd8wdxizv0z3adxlxx6bzfx1376qzc4bwbwrdln4p7fc975m"))))
      (arguments
       '(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'bootstrap
             (lambda _ (invoke "sh" "bootstrap"))))))
      (inputs (list guile-3.0-latest))
      (native-inputs (list autoconf automake pkg-config)))))

(define haunt*
  (let ((version (package-version haunt))
        (revision "0")
        (commit "8b4b14dfa80582632aa74ac1f34c7c428f0eb423"))
    (package
     (inherit haunt)
     (version (git-version version revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/haunt.git")
                    (commit commit)))
              (sha256
               (base32
                "1rhffhzg4ljjbsbijns4gg1cibvdiw2gcv1pmmd94gcmigh60g1m"))))
     (native-inputs
      (list automake autoconf pkg-config texinfo))
     (inputs
      (list rsync guile-3.0-latest))
     (arguments
      (substitute-keyword-arguments (package-arguments haunt)
        ((#:phases phases)
         `(modify-phases ,phases
            (add-after 'unpack 'bootstrap
              (lambda _
                (invoke "sh" "bootstrap"))))))))))

(packages->manifest
  (append
    (list guile-3.0-latest guile-syntax-highlight* haunt*)))
