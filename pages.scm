;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023-2024 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;;
;;; Some code borrowed from dthompson.us and fossandcrafts.org website
;;; Copyright © 2018-2021 David Thompson <davet@gnu.org>
;;; Copyright © 2016-2022 Christine Lemmer-Webber <cwebber@dustycloud.org>

(define-module (pages)
  #:use-module (haunt artifact)
  #:use-module (haunt builder blog)
  #:use-module (haunt html)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (haunt utils)
  #:use-module (web uri)
  #:use-module (haunt page)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (theme)
  #:use-module (utils)
  #:export (%collections
            index-page
            blog-page
            projects-page
            contacts-page
            weblabels-page))

(define %home-page-links 2)

(define foss
  `((,(link "mobilizon-reshare" "https://github.com/Tech-Workers-Coalition-Italia/mobilizon-reshare")
     "Supercharge your event promotion strategy as an organization by automating your social media publishing through an application that can be run on your server, giving you full control and privacy.")
    (,(link "guile-sparql" "https://github.com/roelj/guile-sparql")
     "A SPARQL module for Guile Scheme to query an RDF store. Additionally, it provides an interface to write SPARQL queries using S-expressions.")))

(define projects
  `((,(link "SOPS Guix" "https://github.com/fishinthecalculator/sops-guix")
     "This project implements secure, atomic, trustless secret provisioning with Guix. It works by putting encrypted secrets in the store and by adding a one-shot Shepherd service that decrypts them at
startup in a ramfs/tmpfs filesystem. This means that clear text secrets never hit the disk and that you can (and actually are encouraged to) check in your SOPS secrets in the same version control system
you use to track you Guix configurations.")
    (,(link "gocix" "https://github.com/fishinthecalculator/gocix")
     "This project aims at providing a community managed library of Guix services. Code from this channel implements a Guix native experience for services that are not yet guixable, through OCI backed
Shepherd services.")
    (,(link "small-guix" "https://codeberg.org/fishinthecalculator/small-guix")
     "My staging area for Guix. It holds stuff I plan to upstream somewhere may it be Guix mainline or a  community channel like "
     ,(link "nonguix" "https://gitlab.com/nonguix/nonguix")
     ". Packages or services like the following were born here and
with time contributed where suitable: "
     ,(link "Google Chrome" "https://gitlab.com/nonguix/nonguix/-/blob/master/nongnu/packages/chrome.scm?ref_type=heads")
     ", "
     ,(link "misc K8s stuff" "https://gitlab.com/nonguix/nonguix/-/blob/master/nongnu/packages/k8s.scm?ref_type=heads")
     ", "
     ,(link "OCI Services" "https://guix.gnu.org/en/manual/devel/en/guix.html#index-oci_002dcontainer_002dservice_002dtype")
     ", "
     ,(link "Restic-based backups" "https://guix.gnu.org/en/manual/devel/en/guix.html#index-restic_002dbackup_002dconfiguration")
     ", "
     ,(link "Guix Home dotfiles" "https://guix.gnu.org/en/manual/devel/en/guix.html#index-home_002ddotfiles_002dservice_002dtype")
     ".")))

(define research
  `((,(link "Sample Efficient Reinforcement Learning in Minecraft" "https://codeberg.org/fishinthecalculator/MineRL-Agents/src/branch/master/doc/rainbow_2/README.md")
     "Research project with the aim of creating a Rainbow agent (a specific kind of Deep Q-Network) that can
 accurately collect wooden blocks on Minecraft. It involved agent conﬁguration and training,
 performance validation through standard metrics and generating explanations of adopted
 policies (i.e. saliency maps).")))

(define (format-projects projects)
  (map (lambda (project)
          `(p ,(first project)
              " - "
              ,@(cdr project)))
       projects))

(define (projects-component name id projects)
  `((h1 (@ (id ,id))
        ,name)
    ,(format-projects projects)))

(define (index-content site posts)
 `((h1 "Giacomo Leidi")
   (blockquote
    "My personal space on the web. Mostly about FOSS and technological autonomy.")
   ,(projects-component
     "Projects"
     "projects"
     (take projects (min %home-page-links (length projects))))
   ,(projects-component
     "FOSS"
     "foss"
     (take foss %home-page-links))))

(define (blog-content site posts)
  `((h1 "Recent posts")
    (ul ,@(map (lambda (post)
                 (post-preview post site))
               (posts/reverse-chronological posts)))))

(define (projects-content)
  `(,(projects-component "Projects"
                         "projects"
                         projects)
    ,(projects-component "FOSS"
                         "foss"
                         foss)
    ,(projects-component "Research Projects"
                         "research"
                         research)))

(define contacts-content
  (lambda _
    `((h1 (@ (id "contacts"))
          "Contacts")
      (h2 "Reach out")
      (p
       ,(link "Email"
              "mailto:goodoldpaul@autistici.org")
       " - "
       ,(link "Telegram"
              "https://t.me/paulbutgold"))
      (h2 "Social Media")
      (p
       ,(link "Codeberg"
              "https://codeberg.org/fishinthecalculator")
       " - "
       ,(link "Github"
              "https://github.com/fishinthecalculator"))
      (p
       ,(link "mastodon.bida.im"
              "https://mastodon.bida.im/@paulbutgold")))))

(define weblabels-content
  (lambda _
    (define (make-weblabel script-name)
      (let ((script-url
             (local-script-url script-name)))
        `((td
           (a (@ (href ,script-url))
              ,script-url))
          (td
           ,(link "GNU General Public License 3.0 or later"
                  "https://www.gnu.org/licenses/gpl-3.0.html")))))
    `((h1 (@ (id "weblabels"))
       "JavaScript Web Labels")
      (table
       (tr
        ,@(make-weblabel "dark"))))))

(define (index-page site posts)
  (static-page
   site
   "index.html"
   (index-content site posts)))

(define (blog-page site posts)
  (static-page
   site
   "/blog/index.html"
   (blog-content site posts)))

(define (projects-page site posts)
  (static-page
   site
   "/projects.html"
   (projects-content)))

(define (contacts-page site posts)
  (static-page
   site
   "/contacts.html"
   (contacts-content)))

(define (weblabels-page site posts)
  (static-page
   site
   "/weblabels.html"
   (weblabels-content)))
