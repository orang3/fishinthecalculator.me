title: Tame Docker selfhosting with Guix
date: 2023-12-10 16:20
tags: guix, docker, automation, selfhosted
summary: Automate Docker container provisioning with Guix's `oci-container-service-type`.
---

Many applications are packaged in [OCI](https://opencontainers.org)/Docker images but not in Guix. A good subset of them is written either in NodeJS, Go, Rust or languages that, as a general approach, encourage applications to have huge dependency graphs.

Let's take Grafana as an example. [According to its `package.json`](https://github.com/grafana/grafana/blob/main/package.json) Grafana's web frontend has more than 180 direct dependencies and more than 170 build time dependencies, for a grand total of ~350 direct dependencies. This is excluding all transitive dependencies. In the same way, Grafana's [Go backend](https://github.com/grafana/grafana/blob/main/go.mod) has ~440 direct dependencies. Some of them may be optional, but go figure.

This phenomenon is not uncommon in modern software development, it's not that Grafana is doing worse than everyone else. Yet it's quite problematic, it clearly shows that nobody put the effort of auditing or even reviewing the dependency graph of one of the most used dashboarding application in the industry.

Dissecting the interests which prevent the investment in building software products with a sustainable maintenance process is a topic for another post, the point is that the Guix project accepts package contributions that comply to very strict standards in term of licensing and other criteria, such as whether the package and its dependencies can be completely built from source. It's the reason why practically no NodeJS application (or even web applications with complex frontends, such as Grafana) can be upstreamed to Guix. It is not clear [whether they will ever be](https://dustycloud.org/blog/javascript-packaging-dystopia/), since especially in the Javascript world packages suffer from cyclical dependency which complicate the packaging process even more.

It is very painful to have to package whole dependency graphs just to use some service on the Guix System. This is especially true when you have a goal that you want to achieve and you could use Guix to achieve it, but it does not necessarily involve sending a bunch of patches upstream. It could be that you want to start some Matrix community around Guix or other people-sized technologies, you may want to self host a personal cloud with Nextcloud or maybe you want to setup an accessible monitoring dashboard for your services with Grafana and Prometheus. These are all softwares that do not follow the best sustainability practices, but they are pretty useful to use while we build the alternative technology (also called alt-tech) that we need to survive sustainably in our digital lives.

## Container taming on Guix

If you use `docker compose` on the Guix System, you end up having two different interfaces to manage your system services: Shepherd and Docker. The `oci-container-service-type` aims at [implementing Shepherd Services](https://guix.gnu.org/en/manual/devel/en/guix.html#index-oci_002dcontainer_002dservice_002dtype) that look and feel native (so you can configure and manage them with the usual consistent interface that Guix exposes) but under the hood are implemented as `docker run` (or `docker rm`) invokations, so that you don't have to package those huge dependency graphs just to start self hosting on Guix.

Next you can find an example of how run Prometheus on the Guix System through the `oci-container-service-type`. You just need to add

```scheme
(use-modules (gnu services docker)
             (gnu services monitoring)
             (guix gexp))

(define prometheus.yml
  (plain-file "prometheus.yml"
              "global:
  scrape_interval: 30s
  scrape_timeout: 12s

scrape_configs:
  - job_name: prometheus
    metrics_path: /metrics
    static_configs:
      - targets: ['localhost:9090','localhost:9100']\n"))

(operating-system
  [...]
  (services
    (list
      [...]
      ;; Prometheus node exporter
      (service prometheus-node-exporter-service-type)
      ;; Prometheus OCI backed Shepherd service
      (simple-service oci-container-service-type
                      (list
                        (oci-container-configuration
                          (command
                            '("--web.enable-lifecycle"
                              "--config.file=/etc/prometheus/prometheus.yml"
                              "--web.enable-admin-api"))
                          (image "prom/prometheus:v2.45.0")
                          (ports
                            `(("9000" . "9000")
                              ("9090" . "9090")))
                          (volumes
                            `(("/var/lib/prometheus" . "/prometheus")
                              (,prometheus.yml . "/etc/prometheus/prometheus.yml:ro")))))))))
```

to the `services` field of your `operating-system` record. In this example it's also shown how to install the Prometheus node exporter to collect metrics from the host.

This approach obviously is lacking in reproducibility and bootstrappability, but in my experience often users need some software that is not yet guixable (i.e. possible to include in Guix upstream) so they choose to use Nix or something else entirely.

## What's next?

The `oci-container-service-type` implements Shepherd services through Docker containers, but a Shepherd service is only a small block in the implementation of a nicely integrated Guix System service. To provide a secure, consistent and integrated experience a Guix System service may declare user accounts, to allow for less then root authority execution, or it may initialize some state upon activation: all things for which an additional service extension is required.

This is why I'm implementing a [library of (hopefully) community maintained Guix System services](https://github.com/fishinthecalculator/gocix) for many common self hostable applications such as Matrix Conduit, Forgejo, Grafana and more. These services try to be as similar as possible to an upstream provided service, in the hope of being upstreamed as soon as the underlying dependency graph is packaged.
