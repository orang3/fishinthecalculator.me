#!/usr/bin/env sh

set -eu

log () {
    echo "================================= Downloading ${1} v${2} to ${3} ================================="
}

# Normalize.css

here="$(cd "$(dirname "$0")" && pwd)"
project_root="$(dirname "${here}")"
css_directory="${project_root}/static/css"
images_directory="${project_root}/static/images"

normalize_version="8.0.1"
normalize_url="https://necolas.github.io/normalize.css/${normalize_version}/normalize.css"
normalize_local_path="${css_directory}/reset.css"

log "normalize.css" "${normalize_version}" "${normalize_local_path}"
curl "$normalize_url" > "$normalize_local_path"

# Terminal.css

terminal_version="0.7.2"
terminal_url="https://unpkg.com/terminal.css@${terminal_version}/dist/terminal.min.css"
terminal_local_path="${css_directory}/terminal.css"

log "terminal.css" "${terminal_version}" "${terminal_local_path}"
curl "$terminal_url" > "$terminal_local_path"

# Creative Commons Logo

cc_version="4.0"
cc_url="https://licensebuttons.net/l/by-sa/${cc_version}/80x15.png"
cc_local_path="${images_directory}/cc-by-sa-${cc_version}.png"

log "CC BY-SA PNG logo" "${cc_version}" "${cc_local_path}"
curl "$cc_url" > "$cc_local_path"
