// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>
//
// Some of this code was inspired from manueldorso.rip.

(() => {
  const defaultTheme = "light"
  const body = document.body
  const lightBulb = document.getElementById("bulb")

  const applyTheme = (state) => {
    if (state === "dark") {
      localStorage.setItem("theme", "dark")
      body.setAttribute("data-theme", "dark")
    } else {
      localStorage.setItem("theme", "light")
      body.removeAttribute("data-theme")
    }
  }

  const toggleTheme = () => {
    const state = localStorage.getItem("theme")
    if (state === "dark") {
      applyTheme("light")
    } else {
      applyTheme("dark")
    }
  }

  lightBulb.addEventListener("click", toggleTheme)

  window.addEventListener("load", () => {
    const prefersDarkTheme = window.matchMedia("(prefers-color-scheme: dark)").matches
    let theme = prefersDarkTheme ? "dark" : defaultTheme
    if ("theme" in localStorage) {
      theme = localStorage.getItem("theme")
    }
    applyTheme(theme)
  })
})()
