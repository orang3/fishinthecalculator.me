;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2018-2021 David Thompson <davet@gnu.org>
;;; Copyright © 2016-2022 Christine Lemmer-Webber <cwebber@dustycloud.org>
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (theme)
  #:use-module (haunt artifact)
  #:use-module (haunt builder blog)
  #:use-module (haunt html)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (haunt utils)
  #:use-module (web uri)
  #:use-module (haunt page)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (utils)
  #:export (fishinthecalculator-theme
            post-preview
            base-tmpl
            static-page
            project-page))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  `(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      ,(base-image "cc-by-sa-4.0.png"
                 "Creative Commons Attribution Share-Alike 4.0 International")))

(define (header-menu)
  `(("blog" ,(prefix-url "/blog"))
    ("projects" ,(prefix-url "/projects.html"))
    ("contacts" ,(prefix-url "/contacts.html"))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define (post-preview post site)
  `(li (a (@ (href ,(post-uri site post)))
          ,(post-ref post 'title))
       (div (@ (class "news-feed-content"))
            (div (@ (class "news-feed-item-date"))
                 ,(date->string* (post-date post)))
            ,(first-paragraph post))))

(define post-template
  (lambda* (post #:key post-link)
   `((h1 (@ (class "title"))
      ,(post-ref post 'title))
     (div (@ (class "date"))
          ,(date->string (post-date post)
                         "~B ~d, ~Y"))
     (div "Tags: "
          (ul (@ (class "tags"))
              ,@(map (lambda (tag)
                       `(li (a (@ (href ,(string-append "/feeds/tags/"
                                                        tag ".xml")))
                               ,tag)))
                     (assq-ref (post-metadata post) 'tags))))
     (br)
     (div (@ (class "post"))
          ,(post-sxml post)))))

(define* (base-tmpl site body
                    #:key title)
  `((doctype "html")
    (html (@ (lang "en-US"))
     (head
      (meta (@ (charset "utf-8")))
      (meta (@ (name "viewport")
               (content "width=device-width, initial-scale=1.0")))
      (meta (@ (http-equiv "X-UA-Compatible")
               (content "ie=edge")))
      (meta (@ (name "description")
               (content "My personal space on the web. Mostly about FOSS and technological autonomy.")))
      (title ,(if title
                  (string-append title " -- fishinthecalculator")
                  (site-title site)))
      ;; css
      ,(stylesheet "reset")
      ,(stylesheet "fonts")
      ,(stylesheet "terminal")
      ,(stylesheet "fishinthecalculator"))
     (body (@ (class "terminal"))
           (div (@ (class "container"))
                (header ;; Header menu
                        (div (@ (class "terminal-nav"))
                             (div (@ (class "terminal-logo"))
                                  (div (@ (class "logo terminal-prompt"))
                                       (a (@ (class "no-style")
                                             (href ,(prefix-url "/")))
                                          "fishinthecalculator")))
                             (nav (@ (class "terminal-menu"))
                                  (ul ,@(map
                                         (lambda (item)
                                           (let ((name (first item))
                                                 (url (second item)))
                                             `(li
                                               (a (@ (class "menu-item")
                                                     (href ,url))
                                                  ,name))))
                                         (header-menu))
                                      (li
                                       (a (@ (id "bulb"))
                                          "💡")))))
                        (hr (@ (class "mobilehr"))))
                ,body
                (footer (hr)
                        (p (@ (class "copyright"))
                           "© 2023-2025 Giacomo Leidi"
                           ,%cc-by-sa-button)
                        (p "The text and images on this site are
free culture works available under the " ,%cc-by-sa-link " license.")
                        (p "This website is built with "
                           (a (@ (href "https://dthompson.us/projects/haunt.html"))
                              "Haunt")
                           ", a static site generator written in "
                           (a (@ (href "https://gnu.org/software/guile"))
                              "Guile Scheme")
                           ". Here you can find its "
                           (a (@ (href "https://git.sr.ht/~fishinthecalculator/fishinthecalculator.me"))
                              "source code")
                           " and all "
                           ,(link "Javascript license information"
                                  (prefix-url "/weblabels.html"))
                           ".")
                        (p
                         "Follow me on "
                         (a (@ (rel "me")
                               (href "https://mastodon.bida.im/@paulbutgold"))
                            "Mastodon")
                         "."))
                ,(local-script "dark"))))))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "posts-header"))
         (h3 "recent posts"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define fishinthecalculator-theme
  (theme #:name "fishinthecalculator"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template
         #:pagination-template
         (lambda (site body previous-page next-page)
           `(,@body
             (div (@ (class "paginator"))
              ,(if previous-page
                   `(a (@ (class "paginator-prev") (href ,previous-page)
                        "🡐 Newer"))
                   '())
              ,(if next-page
                   `(a (@ (class "paginator-next") (href ,next-page))
                       "Older 🡒")
                   '()))))))

(define* (static-page site file-name body)
  (serialized-artifact file-name
                       (base-tmpl site body)
                       sxml->html))
