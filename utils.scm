;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2018-2021 David Thompson <davet@gnu.org>
;;; Copyright © 2016-2022 Christine Lemmer-Webber <cwebber@dustycloud.org>
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (utils)
  #:use-module (haunt asset)
  #:use-module (haunt artifact)
  #:use-module (haunt builder blog)
  #:use-module (haunt html)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (haunt utils)
  #:use-module (web uri)
  #:use-module (haunt reader)
  #:use-module (haunt reader commonmark)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 rdelim)
  #:export (date
            stylesheet
            base-image
            local-script-url
            local-script
            anchor
            link
            raw-snippet
            %site-prefix
            prefix-url
            post-uri)
  #:replace (link))


(define %site-prefix (make-parameter ""))


(define (date year month day)
  "Create a SRFI-19 date for the given YEAR, MONTH, DAY"
  (let ((tzoffset (tm:gmtoff (localtime (time-second (current-time))))))
    (make-date 0 0 0 0 day month year tzoffset)))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (style "width: auto;")
           (alt ,alt))))

(define (local-script-url name)
  (prefix-url (string-append "/static/js/" name ".js")))

(define (local-script name)
  `(script (@ (src ,(local-script-url name))
              (defer "defer"))))

(define* (anchor content #:optional (uri content))
  `(a (@ (href ,uri)) ,content))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define (raw-snippet code)
  `(pre (code ,(if (string? code) code (read-string code)))))

(define (prefix-url url)
  (string-append (%site-prefix) url))

(define (post-uri site post)
  (prefix-url
   (string-append "/blog/" (site-post-slug site post) ".html")))
